export class ProductView {
    constructor(element, model) {
        this.element = element;
        this.model = model;
        this.onAddToCart = null;

        this.initialize();
    }

    initialize() {
      const locationCollection = window.location.href.split("/");
      const productId = !!locationCollection.length &&
                        locationCollection[locationCollection.length - 1];

      !!productId && this.model.getProduct(productId)
            .then((productItem) => {
                this.renderProduct(productItem);
            });
    }

    renderProduct(product) {
        this.element.innerHTML = "";

        const productElement = this.getProductTemplate(product);
        this.element.appendChild(productElement);
        this.addEventListener(productElement, ".cart > a.button", "click", this.addToCart);
    }

    addToCart = (event) => {
        const element = !event.target.attributes.productId?
               event.target.parentElement:
               event.target;
        const productId = element.attributes.productId.value;
        this.onAddToCart(productId);
    }

    addEventListener(element, selector, eventType, eventListenerFn) {
        const el = element.querySelector(selector);
        el.addEventListener(eventType, eventListenerFn);
    }

    removeEventListener(element, selector, eventType, eventListenerFn) {
        const el = element.querySelector(selector);
        el.removeEventListener(eventType, eventListenerFn);
    }

    getProductTemplate(productItem) {
      let element = document.createElement("div");
      element.innerHTML = `
      <div class="product-page content-wrapper">
                <div class="pictures">
                    <div class="more-pictures">
                    </div>
                </div>
                <div class="details">
                    <h1 class="name">${productItem.name}</h1>
                    <p class="description">${productItem.description}</p>
                    <div class="add-to-cart-wrapper">
                        <div class="price">
                            <span>${productItem.price} &euro;</span>
                        </div>
                        <div class="quantity-wrapper">
                            <div class="quantity">Quantity: <input type="number" value="${productItem.quantity}" min="1" max="100">
                            </div>
                            <div class="cart"><a class="button" productId=${productItem.productId}>
                              <i class="fa fa-cart-plus"></i><span>Add to cart</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;

       !!productItem.images &&
       !!productItem.images.length &&
        this.addImagesToTemplate(element,productItem);

       return element.firstElementChild;
     }

     addImagesToTemplate(element,productItem){
     const morePicturesElement = element.querySelector(".more-pictures");
     let imageElem = document.createElement("img");
     imageElem.src = productItem.images[0];
     morePicturesElement.parentElement.insertBefore(imageElem, morePicturesElement);

     for(let i = 1; i < productItem.images.length;i++){
       imageElem = document.createElement("img");
       imageElem.src = productItem.images[i];
       morePicturesElement.appendChild(imageElem);
     }
  }
}
