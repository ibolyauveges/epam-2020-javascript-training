import { ProductController } from "./product.controller.js";
import { ProductView } from "./product.view.js";
import { ProductService } from "../../services/product.service.js";

const productPageElement = document.getElementsByTagName('main')[0];
const productService = new ProductService();

new ProductController(
    new ProductView(productPageElement, productService),
    productService
);
